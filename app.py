__author__ = 'Michael'

from bottle import *
from pymongo import *
import hashlib
import uuid


COOKIE_SECRET = "kdL7O3sIx3sPaG9HpWioZ59iqarIr2qR8w0"


@route('/')
def index():
    return template('home.tpl', title="Home",
                    username=request.get_cookie("username", secret=COOKIE_SECRET),
                    message=None)


@route('/item/list')
def item_list():
    username = request.get_cookie('username', secret=COOKIE_SECRET)
    db = get_mongodb()
    items = db.items.find()
    return template('itemlist.tpl', items=items, username=username, message=None)


@get('/additem')
def get_item_add():
    username = request.get_cookie('username', secret=COOKIE_SECRET)
    if username is None or not check_admin(username):
        return redirect('/item/list')

    return template('add_item.tpl', username=request.get_cookie("username", secret=COOKIE_SECRET), message=None)


@post('/additem')
def add_item():
    username = request.get_cookie('username', secret=COOKIE_SECRET)
    if username is None or not check_admin(username):
        return redirect('/item/list')

    name = request.forms.get('name')
    price = request.forms.get('price')
    picture = request.forms.get('picture')
    quantity = request.forms.get('quantity')
    description = request.forms.get('description')

    db = get_mongodb()
    db.items.insert({'_id': name,
                     'picture': picture,
                     'quantity': quantity,
                     'description': description,
                     'price': price})

    return redirect('/item/{0}'.format(name))


@get('/item/<item_id>')
def get_item(item_id):
    username = request.get_cookie("username", secret=COOKIE_SECRET)
    db = get_mongodb()
    item = db.items.find_one({'_id': item_id})

    if item is None:
        redirect('/item/list')

    return template('item.tpl', username=username, item=item, message=None)


@get('/update/<item_id>')
def get_update_item(item_id):
    username = request.get_cookie("username", secret=COOKIE_SECRET)

    if not check_admin(username):
        redirect('/item/{0}'.format(item_id))

    db = get_mongodb()
    item = db.items.find_one({'_id': item_id})

    if item is None:
        redirect('/item/list')

    return template('updateitem.tpl', username=username, item=item, message=None)


@post('/update')
def post_update_item():
    username = request.get_cookie("username", secret=COOKIE_SECRET)

    if not check_admin(username):
        redirect('/')

    name = request.forms.get('name')
    price = request.forms.get('price')
    picture = request.forms.get('picture')
    quantity = request.forms.get('quantity')
    description = request.forms.get('description')

    db = get_mongodb()
    item = db.items.find_one(name)
    item['price'] = price
    item['quantity'] = quantity
    item['description'] = description

    db.items.update({'_id': name}, item)
    return redirect('/item/{0}'.format(name))


@get('/login')
def get_login():
    return template('login.tpl', username=None, message=None)


@post('/login')
def post_login():
    username = request.forms.get("username")
    password = request.forms.get("password")

    user = get_user_info(username)

    if user is None:
        return template('login.tpl', username=None, message="Couldn't find username. Try registering first.")

    if not check_password(password, user['password'], user['salt']):
        return template('login.tpl', username=None, message="Incorrect login. Try again.")

    response.set_cookie("username", username, secret=COOKIE_SECRET)
    return template('home.tpl', username=username, message="Welcome, {0}!".format(username))


@route('/logout')
def logout():
    response.delete_cookie('username', secret=COOKIE_SECRET)
    return template('home.tpl', username=None, message='Successfully logged out.')

@get('/register')
def get_register():
    return template('register.tpl', message=None)


@post('/register')
def post_register():
    username = request.forms.get("username")
    user_email = request.forms.get("email")
    irc = request.forms.get("irc")
    seat = request.forms.get("seat")
    password = request.forms.get("password")
    confirm_password = request.forms.get("confirm_password")

    if password != confirm_password:
        return template("register.tpl", message="Passwords don't match.")

    salt, hashed_password = hash_password(password)

    new_user = {"_id": username,
                "email": user_email,
                "irc": irc,
                "seat": seat,
                "password": hashed_password,
                "salt": salt,
                "admin": "0"}

    db = get_mongodb()
    db.users.insert(new_user)
    return template('login.tpl', message="Registered! Time to log in!")


@route('/static/<filename>')
def get_file(filename):
    return static_file(filename, root="C:\\Users\\Michael\\PyCharmProjects\\lanstore")


def get_mongodb():
    client = MongoClient('mongodb://localhost:27017/')
    return client.store


def hash_password(password):
    password = password.encode('utf8')
    salt = uuid.uuid4().bytes
    hashed_password = hashlib.sha512(password + salt).hexdigest()
    return salt, hashed_password


def check_password(password, hash, salt):
    password = password.encode('utf8')
    check_hash = hashlib.sha512(password + salt).hexdigest()
    return check_hash == hash


def get_user_info(username):
    db = get_mongodb()
    return db.users.find_one({"_id": username})


def check_admin(username):
    db = get_mongodb()
    user = db.users.find_one({"_id": username})

    if user['admin'] == 1:
        return True
    return False


run(host='store.megahyper.ninja', port=80)