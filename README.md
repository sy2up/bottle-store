# README #

Welcome to the store! Well, the code for it anyways. Its still really rough around the edges and is missing a lot of features at the moment. Right now it really only shows a list of products, product details, and lets you add products.


### What is this repository for? ###

* Simple web store front written in bottle.py
* Uses a MongoDB database for the backend


### Current Goals/Stories ###
* Users should have a shopping cart
* Users should have a user control panel
* Basic checkout page that lists all items in cart and creates at least a subtotal
* Invoice tracking for previous purchases from a customer
* Inventory update upon purchase confirmation
* Major UI overhaul and update


### How do I get set up? ###

I'll be making a Vagrantfile and Docker container for this app soon, as it relies on some static paths for static files.
Until then just checkout the source code, update the strings that make sense (static files, cookie secret), and run MongoDB locally.

### Who do I talk to? ###

* Michael (sy2up)