% include('base.tpl', title='Home', username=username, message=message)
<link rel="stylesheet" type="text/css" href="/static/item.css" />
<div class="item">
    <div class="item_name">
        <center><h1>{{item['_id']}}</h1></center>
    </div>
    <div class="item_price">
        <h2>Price: ${{item['price']}}</h2>
    </div>
    <div class="item_quantity">
        <h2>Quantity: {{item['quantity']}}</h2>
    </div>
    <div class="item_description">
        {{item['description']}}
    </div>
</div>