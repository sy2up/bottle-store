% include('base.tpl', title='Register', username=None, message=None)
<link rel="stylesheet" type="text/css" href="/static/register.css" />
<div id="registerBox">
    <div class="registerBox">
        <form action="/register" method="post" >
            <div class="formRow">
                <div class="formLabel">
                    Username:
                </div>
                <div class="formField">
                    <input name="username" type="text" required="true">
                </div>
            </div>
            <div class="formRow">
                <div class="formLabel">
                    Email:
                </div>
                <div class="formField">
                    <input name="email" type="email" align="right" required="true">
                </div>
            </div>
            <div class="formRow">
                <div class="formLabel">
                    IRC Handle:
                </div>
                <div class="formField">
                    <input name="irc" type="text" required="true">
                </div>
            </div>
            <div class="formRow">
                <div class="formLabel">
                    Seat:
                </div>
                <div class="formField">
                    <input name="seat" type="text" required="true">
                </div>
            </div>
            <div class="formRow">
                <div class="formLabel">
                    Password:
                </div>
                <div class="formField">
                    <input name="password" type="password" required="true">
                </div>
            </div>
            <div class="formRow">
                <div class="formLabel">
                    Confirm Password:
                </div>
                <div class="formField">
                    <input name="confirm_password" type="password" required="true">
                </div>
            </div>
            <center><input class="registerButton" type="submit" value="Register"></center>
        </form>
    </div>
</div>