% include('base.tpl', title='Home', username=username, message=message)
<link rel="stylesheet" type="text/css" href="/static/item.css" />
% for item in items:
<div class="itemlist">
    <div class="item_name">
        <a href="/item/{{item['_id']}}">{{item['_id']}}</a>
    </div>
    <div class="item_quantity">
        Quantity: {{item['quantity']}}
    </div>
    <div class="item_price">
        ${{item['price']}}
    </div>
</div>
% end