<link rel="stylesheet" type="text/css" href="/static/base.css" />
<title>{{title}} ::. The Store</Title>
<div id="top-bar">
%if username == None:
    <center><a href="/">Home</a> - <a href="/item/list">Item List</a> -  <a href="/login">Login</a></center>
%else:
    <center><a href="/">Home</a> - <a href="/item/list">Item List</a> - <a href="/logout">Logout</a></center>
%end
</div>
%if message != None:
<div id="messagebox">
    <center>{{message}}</center>
</div>
%end