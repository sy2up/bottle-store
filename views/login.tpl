% include('base.tpl', title='Login', username=None, message=None)
<link rel="stylesheet" type="text/css" href="/static/login.css" />
<div id="loginbox">
	<center><h2>Sign in to the Store:</h2></center>
    <form action="/login" method="post">
        <div class="formRow">
            <div class ="label">
                Username:
            </div>
            <div class="field">
                <input name="username" type="text" align="right">
            </div>
        </div>
        <div class="formRow">
            <div class="label">
                Password:
            </div>
            <div class="field">
                <input name="password" type="password" align="right">
            </div>
        </div>
        <center><input class="loginButton" type="submit" value="Login"></center>
    </form>
    <center><a href="/register">Click here to make an account</a></center>
</div>