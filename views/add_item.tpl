% include('base.tpl', title='Add New Item', username=username, message=message)
<link rel="stylesheet" type="text/css" href="/static/additem.css" />
<div id="additemBox">
    <form id="additem" action="/additem" method="post">
        <div class="formRow">
            <div class="formLabel">
                Name:
            </div>
            <div class="formField">
                <input name="name" type="text" required="true">
            </div>
        </div>
        <div class="formRow">
            <div class="formLabel">
                Price:
            </div>
            <div class="formField">
                <input name="price" type="text" required="true">
            </div>
        </div>
        <div class="formRow">
            <div class="formLabel">
                Picture:
            </div>
            <div class="formField">
                <input name="picture" type="file">
            </div>
        </div>
        <div class="formRow">
            <div class="formLabel">
                Quantity:
            </div>
            <div class="formField">
                <input name="quantity" type="number" required="true">
            </div>
        </div>
        <div class="formRow" id="textArea">
            <div class="formLabel">
                Description:
            </div>
            <div class="formField">
                <textarea name="description" type="text" form="additem" required="true" ></textarea>
            </div>
        </div>
        <center><input class="submitButton" type="submit" value="Add Item"></center>
    </form>
</div>