% include('base.tpl', title='Update Item', username=username, message=message)
<link rel="stylesheet" type="text/css" href="/static/additem.css" />
<div id="additemBox">
    <form id="additem" action="/update" method="post">
        <center><h1>{{item['_id']}}</h1></center>
        <input name="name" type="hidden" required="true" value={{item['_id']}}>
        <div class="formRow">
            <div class="formLabel">
                Price:
            </div>
            <div class="formField">
                <input name="price" type="text" required="true" value={{item['price']}}>
            </div>
        </div>
        <div class="formRow">
            <div class="formLabel">
                Quantity:
            </div>
            <div class="formField">
                <input name="quantity" type="number" required="true" value={{item['quantity']}}>
            </div>
        </div>
        <div class="formRow" id="textArea">
            <div class="formLabel">
                Description:
            </div>
            <div class="formField">
                <textarea name="description" type="text" form="additem" required="true" >{{item['description']}}</textarea>
            </div>
        </div>
        <center><input class="submitButton" type="submit" value="Update Item"></center>
    </form>
</div>